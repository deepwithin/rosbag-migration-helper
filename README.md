# outdated bagfile migration

To help update the bag file with updated msg definitions and changed topic paths

Put these packages in your catkin workspace and build it, source it.

Usage: 

`rosrun golden_data change_msg.py`

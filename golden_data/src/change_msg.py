#!/usr/bin/env python3

"""
Module Name
Module Description
author: Deep Patel
email: deep.patel@vimaan.ai
"""

import rospy

from product.msg import Trigger
from trigger.msg import Object_Detected

INPUT_BAG_TOPIC = '/kevlar_loc/0003/Triggerinfo'
OUTPUT_TOPIC = '/Triggerinfo'

class ChangeMsg:
    def __init__(self) -> None:

        rospy.Subscriber(INPUT_BAG_TOPIC, Trigger, self.callback_trig)
        

        self.old_msg = None
        self.seq = 0
        
        # Define publishers
        self.new_pub = rospy.Publisher(OUTPUT_TOPIC, Object_Detected, queue_size=10)
        self.new_msg = Object_Detected()



    def callback_trig(self, data):
        
        self.old_msg = data
        rospy.loginfo("received the old message..")
        self.change_msg_engine()

    
    def change_msg_engine(self):
        """
        Main code for msg reconstruction

        :return: none
        """
        self.seq += 1
        self.new_msg.header.seq = self.seq
        self.new_msg.header.stamp.secs = (self.old_msg.timestamp.secs)
        self.new_msg.header.stamp.nsecs = (self.old_msg.timestamp.nsecs)
        self.new_msg.header.frame_id = ""
        
        self.new_msg.Status = self.old_msg.start
        self.new_msg.source = self.old_msg.source
        
        rospy.loginfo("reconstructing the message ...")
        self.new_pub.publish(self.new_msg)


def main():
    rospy.init_node('change_msg_node')
    rospy.loginfo("change msg node is up.")
    cm = ChangeMsg()
    
    rospy.spin()

if __name__ == '__main__':
    main()